<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B02981">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A declaration of the Parliament of England, concerning proceedings in courts of justice.</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B02981 of text R175097 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing E1497A). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">B02981</idno>
    <idno type="STC">Wing E1497A</idno>
    <idno type="STC">ESTC R175097</idno>
    <idno type="EEBO-CITATION">52612147</idno>
    <idno type="OCLC">ocm 52612147</idno>
    <idno type="VID">179438</idno>
    <idno type="PROQUESTGOID">2240867498</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B02981)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179438)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2789:5)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A declaration of the Parliament of England, concerning proceedings in courts of justice.</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed for Edward Husband, Printer to the Honorable House of Commons,</publisher>
      <pubPlace>London :</pubPlace>
      <date>Febr. 9, 1648.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Initial letter.</note>
      <note>Text of declaration in black letter.</note>
      <note>Order to print dated: Die Jovis, 8 Febr. 1648. Signed: Hen: Scobell, Cleric. Parliament.</note>
      <note>Reproduction of the original in the Harvard Law School Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Courts -- England -- 17th century.</term>
     <term>Judges -- England -- Selection and appointment -- 17th century.</term>
     <term>Broadsides -- England -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>B02981</ep:tcp>
    <ep:estc> R175097</ep:estc>
    <ep:stc> (Wing E1497A). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>A declaration of the Parliament of England, concerning proceedings in courts of justice.</ep:title>
    <ep:author>England and Wales. Parliament</ep:author>
    <ep:publicationYear>1648</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>115</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-08</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-10</date><label>John Pas</label>
        Sampled and proofread
      </change>
   <change><date>2008-10</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B02981-e10">
  <body xml:id="B02981-e20">
   <pb facs="tcp:179438:1" n="13" rend="simple:additions" xml:id="B02981-001-a"/>
   <div type="text" xml:id="B02981-e30">
    <head xml:id="B02981-e40">
     <w lemma="a" pos="d" xml:id="B02981-001-a-0010">A</w>
     <w lemma="declaration" pos="n1" xml:id="B02981-001-a-0020">DECLARATION</w>
     <w lemma="of" pos="acp" xml:id="B02981-001-a-0030">OF</w>
     <w lemma="the" pos="d" xml:id="B02981-001-a-0040">The</w>
     <w lemma="parliament" pos="n1" xml:id="B02981-001-a-0050">Parliament</w>
     <w lemma="of" pos="acp" xml:id="B02981-001-a-0060">of</w>
     <hi xml:id="B02981-e50">
      <w lemma="ENGLAND" pos="nn1" xml:id="B02981-001-a-0070">ENGLAND</w>
      <pc xml:id="B02981-001-a-0080">,</pc>
     </hi>
     <w lemma="concerning" pos="acp" xml:id="B02981-001-a-0090">Concerning</w>
     <w lemma="proceed" pos="n2-vg" xml:id="B02981-001-a-0100">Proceedings</w>
     <w lemma="in" pos="acp" xml:id="B02981-001-a-0110">in</w>
     <w lemma="court" pos="n2" xml:id="B02981-001-a-0120">Courts</w>
     <w lemma="of" pos="acp" xml:id="B02981-001-a-0130">of</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="B02981-001-a-0140">Iustice</w>
     <pc unit="sentence" xml:id="B02981-001-a-0150">.</pc>
    </head>
    <p xml:id="B02981-e60">
     <w lemma="the" pos="d" rend="decorinit" xml:id="B02981-001-a-0160">THe</w>
     <w lemma="parliament" pos="n1" xml:id="B02981-001-a-0170">Parliament</w>
     <w lemma="of" pos="acp" xml:id="B02981-001-a-0180">of</w>
     <hi xml:id="B02981-e70">
      <w lemma="England" pos="nn1" xml:id="B02981-001-a-0190">England</w>
     </hi>
     <w lemma="now" pos="av" xml:id="B02981-001-a-0200">now</w>
     <w lemma="assemble" pos="vvn" xml:id="B02981-001-a-0210">assembled</w>
     <w lemma="do" pos="vvb" xml:id="B02981-001-a-0220">do</w>
     <w lemma="declare" pos="vvi" xml:id="B02981-001-a-0230">Declare</w>
     <pc xml:id="B02981-001-a-0240">,</pc>
     <w lemma="that" pos="cs" xml:id="B02981-001-a-0250">That</w>
     <w lemma="be" pos="vvg" xml:id="B02981-001-a-0260">being</w>
     <w lemma="full" pos="av-j" xml:id="B02981-001-a-0270">fully</w>
     <w lemma="resolve" pos="vvn" xml:id="B02981-001-a-0280">resolved</w>
     <w lemma="to" pos="prt" xml:id="B02981-001-a-0290">to</w>
     <w lemma="maintain" pos="vvi" xml:id="B02981-001-a-0300">maintain</w>
     <w lemma="the" pos="d" xml:id="B02981-001-a-0310">the</w>
     <w lemma="fundamental" pos="j" xml:id="B02981-001-a-0320">Fundamental</w>
     <w lemma="law" pos="n2" xml:id="B02981-001-a-0330">Laws</w>
     <w lemma="of" pos="acp" xml:id="B02981-001-a-0340">of</w>
     <w lemma="this" pos="d" xml:id="B02981-001-a-0350">this</w>
     <w lemma="nation" pos="n1" xml:id="B02981-001-a-0360">Nation</w>
     <w lemma="for" pos="acp" xml:id="B02981-001-a-0370">for</w>
     <w lemma="the" pos="d" xml:id="B02981-001-a-0380">the</w>
     <w lemma="good" pos="j" xml:id="B02981-001-a-0390">good</w>
     <w lemma="of" pos="acp" xml:id="B02981-001-a-0400">of</w>
     <w lemma="the" pos="d" xml:id="B02981-001-a-0410">the</w>
     <w lemma="people" pos="n1" xml:id="B02981-001-a-0420">People</w>
     <pc xml:id="B02981-001-a-0430">;</pc>
     <w lemma="and" pos="cc" xml:id="B02981-001-a-0440">and</w>
     <w lemma="have" pos="vvg" xml:id="B02981-001-a-0450">having</w>
     <w lemma="appoint" pos="vvn" xml:id="B02981-001-a-0460">appointed</w>
     <w lemma="judge" pos="n2" reg="judges" xml:id="B02981-001-a-0470">Iudges</w>
     <w lemma="for" pos="acp" xml:id="B02981-001-a-0480">for</w>
     <w lemma="the" pos="d" xml:id="B02981-001-a-0490">the</w>
     <w lemma="administration" pos="n1" xml:id="B02981-001-a-0500">Administration</w>
     <w lemma="of" pos="acp" xml:id="B02981-001-a-0510">of</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="B02981-001-a-0520">Iustice</w>
     <w lemma="in" pos="acp" xml:id="B02981-001-a-0530">in</w>
     <w lemma="execution" pos="n1" xml:id="B02981-001-a-0540">execution</w>
     <w lemma="thereof" pos="av" xml:id="B02981-001-a-0550">thereof</w>
     <pc xml:id="B02981-001-a-0560">,</pc>
     <w lemma="do" pos="vvb" xml:id="B02981-001-a-0570">do</w>
     <w lemma="expect" pos="vvi" xml:id="B02981-001-a-0580">expect</w>
     <w lemma="that" pos="cs" xml:id="B02981-001-a-0590">that</w>
     <w lemma="they" pos="pns" xml:id="B02981-001-a-0600">they</w>
     <w lemma="proceed" pos="vvb" xml:id="B02981-001-a-0610">proceed</w>
     <w lemma="according" pos="av-j" xml:id="B02981-001-a-0620">accordingly</w>
     <pc unit="sentence" xml:id="B02981-001-a-0630">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="B02981-e80">
   <div type="license" xml:id="B02981-e90">
    <opener xml:id="B02981-e100">
     <dateline xml:id="B02981-e110">
      <date xml:id="B02981-e120">
       <foreign xml:id="B02981-e130" xml:lang="lat">
        <w lemma="die" pos="fla" xml:id="B02981-001-a-0640">Die</w>
        <w lemma="jovis" pos="fla" xml:id="B02981-001-a-0650">Jovis</w>
        <pc xml:id="B02981-001-a-0660">,</pc>
       </foreign>
       <w lemma="8" pos="crd" xml:id="B02981-001-a-0670">8</w>
       <hi xml:id="B02981-e140">
        <w lemma="febr." pos="ab" xml:id="B02981-001-a-0680">Febr.</w>
       </hi>
       <w lemma="1648." pos="crd" xml:id="B02981-001-a-0700">1648.</w>
       <pc unit="sentence" xml:id="B02981-001-a-0710"/>
      </date>
     </dateline>
    </opener>
    <p xml:id="B02981-e150">
     <hi xml:id="B02981-e160">
      <w lemma="order" pos="j-vn" xml:id="B02981-001-a-0720">ORdered</w>
      <w lemma="by" pos="acp" xml:id="B02981-001-a-0730">by</w>
      <w lemma="the" pos="d" xml:id="B02981-001-a-0740">the</w>
      <w lemma="commons" pos="n2" xml:id="B02981-001-a-0750">Commons</w>
      <w lemma="assemble" pos="vvn" xml:id="B02981-001-a-0760">assembled</w>
      <w lemma="in" pos="acp" xml:id="B02981-001-a-0770">in</w>
      <w lemma="parliament" pos="n1" xml:id="B02981-001-a-0780">Parliament</w>
      <pc xml:id="B02981-001-a-0790">,</pc>
      <w lemma="that" pos="cs" xml:id="B02981-001-a-0800">That</w>
      <w lemma="this" pos="d" xml:id="B02981-001-a-0810">this</w>
      <w lemma="declaration" pos="n1" xml:id="B02981-001-a-0820">Declaration</w>
      <w lemma="be" pos="vvb" xml:id="B02981-001-a-0830">be</w>
      <w lemma="forthwith" pos="av" xml:id="B02981-001-a-0840">forthwith</w>
      <w lemma="print" pos="vvn" xml:id="B02981-001-a-0850">Printed</w>
      <w lemma="and" pos="cc" xml:id="B02981-001-a-0860">and</w>
      <w lemma="publish" pos="vvn" xml:id="B02981-001-a-0870">Published</w>
      <pc unit="sentence" xml:id="B02981-001-a-0880">.</pc>
     </hi>
    </p>
    <closer xml:id="B02981-e170">
     <signed xml:id="B02981-e180">
      <w lemma="hen" pos="ab" xml:id="B02981-001-a-0890">Hen:</w>
      <w lemma="Scobell" pos="nn1" xml:id="B02981-001-a-0910">Scobell</w>
      <pc xml:id="B02981-001-a-0920">,</pc>
      <w lemma="cleric" pos="n1" xml:id="B02981-001-a-0930">Cleric</w>
      <pc unit="sentence" xml:id="B02981-001-a-0940">.</pc>
      <w lemma="Parliament'" pos="ab" xml:id="B02981-001-a-0960">Parliament'</w>
      <pc unit="sentence" xml:id="B02981-001-a-0970">.</pc>
     </signed>
    </closer>
   </div>
   <div type="colophon" xml:id="B02981-e190">
    <p xml:id="B02981-e200">
     <hi xml:id="B02981-e210">
      <w lemma="LONDON" pos="nn1" xml:id="B02981-001-a-0980">LONDON</w>
      <pc xml:id="B02981-001-a-0990">:</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="B02981-001-a-1000">Printed</w>
     <w lemma="for" pos="acp" xml:id="B02981-001-a-1010">for</w>
     <hi xml:id="B02981-e220">
      <w lemma="Edward" pos="nn1" xml:id="B02981-001-a-1020">Edward</w>
      <w lemma="husband" pos="n1" xml:id="B02981-001-a-1030">Husband</w>
      <pc xml:id="B02981-001-a-1040">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="B02981-001-a-1050">Printer</w>
     <w lemma="to" pos="acp" xml:id="B02981-001-a-1060">to</w>
     <w lemma="the" pos="d" xml:id="B02981-001-a-1070">the</w>
     <w lemma="honourable" pos="j" reg="honourable" xml:id="B02981-001-a-1080">Honorable</w>
     <w lemma="house" pos="n1" xml:id="B02981-001-a-1090">House</w>
     <w lemma="of" pos="acp" xml:id="B02981-001-a-1100">of</w>
     <w lemma="commons" pos="n2" xml:id="B02981-001-a-1110">Commons</w>
     <pc unit="sentence" xml:id="B02981-001-a-1120">.</pc>
     <w lemma="febr." pos="ab" xml:id="B02981-001-a-1130">Febr.</w>
     <w lemma="9" pos="crd" reg="9" xml:id="B02981-001-a-1140">9.</w>
     <w lemma="1648." pos="crd" xml:id="B02981-001-a-1150">1648.</w>
     <pc unit="sentence" xml:id="B02981-001-a-1160"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
