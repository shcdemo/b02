<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B02939">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>East-India trade</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B02939 of text R174837 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing E104A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B02939</idno>
    <idno type="STC">Wing E104A</idno>
    <idno type="STC">ESTC R174837</idno>
    <idno type="EEBO-CITATION">51784517</idno>
    <idno type="OCLC">ocm 51784517</idno>
    <idno type="VID">174928</idno>
    <idno type="PROQUESTGOID">2240891433</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B02939)</note>
    <note>Transcribed from: (Early English Books Online ; image set 174928)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2706:5)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>East-India trade</title>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1698]</date>
     </publicationStmt>
     <notesStmt>
      <note>Date and place of publication suggested by Wing (2nd ed.).</note>
      <note>Reproduction of original in: Bodleian Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>East India Company -- Early works to 1800.</term>
     <term>Colonial companies -- Great Britain -- Early works to 1800.</term>
     <term>Corporations -- Great Britain -- Early works to 1800.</term>
     <term>Great Britain -- Commerce -- India -- 17th century -- Early works to 1800.</term>
     <term>Broadsides -- England -- London -- 17th century</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>East-India trade. One of the opinions of the Committee appointed to consider how to preserve the East-India trade to the nation, ...</ep:title>
    <ep:author>[no entry]</ep:author>
    <ep:publicationYear>1698</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>472</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-05</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-08</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-09</date><label>John Pas</label>
        Sampled and proofread
      </change>
   <change><date>2008-09</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B02939-t">
  <body xml:id="B02939-e0">
   <div type="text" xml:id="B02939-e10">
    <pb facs="tcp:174928:1" xml:id="B02939-001-a"/>
    <head xml:id="B02939-e20">
     <w lemma="east-india" pos="j" xml:id="B02939-001-a-0010">EAST-INDIA</w>
     <w lemma="trade" pos="n1" xml:id="B02939-001-a-0020">TRADE</w>
    </head>
    <p xml:id="B02939-e30">
     <w lemma="one" pos="crd" xml:id="B02939-001-a-0030">ONe</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-0040">of</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-0050">the</w>
     <w lemma="opinion" pos="n2" xml:id="B02939-001-a-0060">Opinions</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-0070">of</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-0080">the</w>
     <w lemma="committee" pos="n1" xml:id="B02939-001-a-0090">Committee</w>
     <w lemma="appoint" pos="vvd" xml:id="B02939-001-a-0100">appointed</w>
     <w lemma="to" pos="prt" xml:id="B02939-001-a-0110">to</w>
     <w lemma="consider" pos="vvi" xml:id="B02939-001-a-0120">consider</w>
     <w lemma="how" pos="crq" xml:id="B02939-001-a-0130">how</w>
     <w lemma="to" pos="prt" xml:id="B02939-001-a-0140">to</w>
     <w lemma="preserve" pos="vvi" xml:id="B02939-001-a-0150">preserve</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-0160">the</w>
     <w lemma="East-India" pos="nn1" rend="hi" xml:id="B02939-001-a-0170">East-India</w>
     <w lemma="trade" pos="n1" xml:id="B02939-001-a-0180">Trade</w>
     <w lemma="to" pos="acp" xml:id="B02939-001-a-0190">to</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-0200">the</w>
     <w lemma="nation" pos="n1" xml:id="B02939-001-a-0210">Nation</w>
     <pc xml:id="B02939-001-a-0220">,</pc>
     <w lemma="be" pos="vvg" xml:id="B02939-001-a-0230">being</w>
     <pc join="right" xml:id="B02939-001-a-0240">(</pc>
     <w lemma="as" pos="acp" xml:id="B02939-001-a-0250">as</w>
     <w lemma="discourse" pos="vvn" xml:id="B02939-001-a-0260">discoursed</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-0270">of</w>
     <w lemma="abroad" pos="av" xml:id="B02939-001-a-0280">abroad</w>
     <pc xml:id="B02939-001-a-0290">)</pc>
     <w lemma="that" pos="cs" xml:id="B02939-001-a-0300">that</w>
     <w lemma="a" pos="d" xml:id="B02939-001-a-0310">a</w>
     <w lemma="stock" pos="n1" xml:id="B02939-001-a-0320">Stock</w>
     <w lemma="not" pos="xx" xml:id="B02939-001-a-0330">not</w>
     <w lemma="less" pos="dc" xml:id="B02939-001-a-0340">less</w>
     <w lemma="than" pos="cs" xml:id="B02939-001-a-0350">than</w>
     <w lemma="fifteen" pos="crd" xml:id="B02939-001-a-0360">Fifteen</w>
     <w lemma="huudred" pos="j" xml:id="B02939-001-a-0370">Huudred</w>
     <w lemma="thousand" pos="crd" xml:id="B02939-001-a-0380">Thousand</w>
     <w lemma="pound" pos="n2" xml:id="B02939-001-a-0390">Pounds</w>
     <pc xml:id="B02939-001-a-0400">;</pc>
     <w lemma="and" pos="cc" xml:id="B02939-001-a-0410">and</w>
     <w lemma="not" pos="xx" xml:id="B02939-001-a-0420">not</w>
     <w lemma="exceed" pos="vvg" xml:id="B02939-001-a-0430">exceeding</w>
     <w lemma="two" pos="crd" xml:id="B02939-001-a-0440">Two</w>
     <w lemma="million" pos="crd" xml:id="B02939-001-a-0450">Millions</w>
     <pc xml:id="B02939-001-a-0460">,</pc>
     <w lemma="shall" pos="vmb" xml:id="B02939-001-a-0470">shall</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-0480">be</w>
     <w lemma="raise" pos="vvn" xml:id="B02939-001-a-0490">raised</w>
     <w lemma="by" pos="acp" xml:id="B02939-001-a-0500">by</w>
     <w lemma="subscription" pos="n2" xml:id="B02939-001-a-0510">Subscriptions</w>
     <w lemma="for" pos="acp" xml:id="B02939-001-a-0520">for</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-0530">the</w>
     <w lemma="carry" pos="vvg" xml:id="B02939-001-a-0540">carrying</w>
     <w lemma="on" pos="acp" xml:id="B02939-001-a-0550">on</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-0560">of</w>
     <w lemma="that" pos="d" xml:id="B02939-001-a-0570">that</w>
     <w lemma="trade" pos="n1" xml:id="B02939-001-a-0580">Trade</w>
     <pc xml:id="B02939-001-a-0590">,</pc>
     <w lemma="the" pos="d" xml:id="B02939-001-a-0600">the</w>
     <w lemma="true" pos="j" xml:id="B02939-001-a-0610">true</w>
     <w lemma="settle" pos="n1-vg" xml:id="B02939-001-a-0620">settling</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-0630">of</w>
     <w lemma="which" pos="crq" xml:id="B02939-001-a-0640">which</w>
     <pc xml:id="B02939-001-a-0650">,</pc>
     <w lemma="as" pos="acp" xml:id="B02939-001-a-0660">as</w>
     <w lemma="it" pos="pn" xml:id="B02939-001-a-0670">it</w>
     <w lemma="may" pos="vmb" xml:id="B02939-001-a-0680">may</w>
     <w lemma="probable" pos="av-j" xml:id="B02939-001-a-0690">probably</w>
     <w lemma="prove" pos="vvi" xml:id="B02939-001-a-0700">prove</w>
     <w lemma="to" pos="acp" xml:id="B02939-001-a-0710">to</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-0720">the</w>
     <w lemma="mighty" pos="j" xml:id="B02939-001-a-0730">mighty</w>
     <w lemma="advantage" pos="n1" xml:id="B02939-001-a-0740">Advantage</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-0750">of</w>
     <w lemma="such" pos="d" xml:id="B02939-001-a-0760">such</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-0770">the</w>
     <w lemma="subscriber" pos="n2" xml:id="B02939-001-a-0780">Subscribers</w>
     <w lemma="to" pos="acp" xml:id="B02939-001-a-0790">to</w>
     <w lemma="it" pos="pn" xml:id="B02939-001-a-0800">it</w>
     <pc xml:id="B02939-001-a-0810">;</pc>
     <w lemma="so" pos="av" xml:id="B02939-001-a-0820">so</w>
     <w lemma="it" pos="pn" xml:id="B02939-001-a-0830">it</w>
     <w lemma="can" pos="vmb" xml:id="B02939-001-a-0840">can</w>
     <w lemma="bring" pos="vvi" xml:id="B02939-001-a-0850">bring</w>
     <w lemma="no" pos="dx" xml:id="B02939-001-a-0860">no</w>
     <w lemma="benefit" pos="n1" xml:id="B02939-001-a-0870">Benefit</w>
     <w lemma="but" pos="acp" xml:id="B02939-001-a-0880">but</w>
     <w lemma="a" pos="d" xml:id="B02939-001-a-0890">a</w>
     <w lemma="prejudice" pos="n1" xml:id="B02939-001-a-0900">Prejudice</w>
     <w lemma="rather" pos="avc" xml:id="B02939-001-a-0910">rather</w>
     <pc xml:id="B02939-001-a-0920">,</pc>
     <w lemma="to" pos="prt" xml:id="B02939-001-a-0930">to</w>
     <w lemma="all" pos="d" xml:id="B02939-001-a-0940">all</w>
     <w lemma="other" pos="pi2" xml:id="B02939-001-a-0950">others</w>
     <w lemma="not" pos="xx" xml:id="B02939-001-a-0960">not</w>
     <w lemma="therein" pos="av" xml:id="B02939-001-a-0970">therein</w>
     <w lemma="concern" pos="vvn" xml:id="B02939-001-a-0980">concerned</w>
     <pc unit="sentence" xml:id="B02939-001-a-0990">.</pc>
     <w lemma="now" pos="av" xml:id="B02939-001-a-1000">Now</w>
     <pc xml:id="B02939-001-a-1010">,</pc>
    </p>
    <p xml:id="B02939-e50">
     <w lemma="this" pos="d" xml:id="B02939-001-a-1020">This</w>
     <w lemma="not" pos="xx" xml:id="B02939-001-a-1030">not</w>
     <w lemma="yet" pos="av" xml:id="B02939-001-a-1040">yet</w>
     <w lemma="be" pos="vvg" xml:id="B02939-001-a-1050">being</w>
     <w lemma="resolve" pos="vvn" xml:id="B02939-001-a-1060">Resolved</w>
     <w lemma="in" pos="acp" xml:id="B02939-001-a-1070">in</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-1080">the</w>
     <w lemma="house" pos="n1" xml:id="B02939-001-a-1090">House</w>
     <pc xml:id="B02939-001-a-1100">,</pc>
    </p>
    <p xml:id="B02939-e60">
     <label xml:id="B02939-e70">
      <w join="right" lemma="it" pos="pn" xml:id="B02939-001-a-1110">'T</w>
      <w join="left" lemma="be" pos="vvz" xml:id="B02939-001-a-1111">is</w>
      <w lemma="humble" pos="av-j" xml:id="B02939-001-a-1120">Humbly</w>
      <w lemma="propose" pos="vvn" xml:id="B02939-001-a-1130">Proposed</w>
      <pc unit="sentence" xml:id="B02939-001-a-1140">.</pc>
     </label>
    </p>
    <p xml:id="B02939-e80">
     <w lemma="that" pos="cs" xml:id="B02939-001-a-1150">That</w>
     <w lemma="a" pos="d" xml:id="B02939-001-a-1160">a</w>
     <w lemma="new" pos="j" xml:id="B02939-001-a-1170">New</w>
     <w lemma="company" pos="n1" xml:id="B02939-001-a-1180">Company</w>
     <w lemma="may" pos="vmb" xml:id="B02939-001-a-1190">may</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-1200">be</w>
     <w lemma="establish" pos="vvn" xml:id="B02939-001-a-1210">Established</w>
     <w lemma="according" pos="j" xml:id="B02939-001-a-1220">according</w>
     <w lemma="to" pos="acp" xml:id="B02939-001-a-1230">to</w>
     <w lemma="such" pos="d" xml:id="B02939-001-a-1240">such</w>
     <w lemma="regulation" pos="n2" xml:id="B02939-001-a-1250">Regulations</w>
     <w lemma="as" pos="acp" xml:id="B02939-001-a-1260">as</w>
     <w lemma="have" pos="vvi" xml:id="B02939-001-a-1270">have</w>
     <w lemma="be" pos="vvn" xml:id="B02939-001-a-1280">been</w>
     <w lemma="vote" pos="vvd" xml:id="B02939-001-a-1290">Voted</w>
     <w lemma="in" pos="acp" xml:id="B02939-001-a-1300">in</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-1310">the</w>
     <w lemma="committee" pos="n1" xml:id="B02939-001-a-1320">Committee</w>
     <pc xml:id="B02939-001-a-1330">,</pc>
     <w lemma="and" pos="cc" xml:id="B02939-001-a-1340">and</w>
     <w lemma="shall" pos="vmb" xml:id="B02939-001-a-1350">shall</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-1360">be</w>
     <w lemma="agree" pos="vvn" xml:id="B02939-001-a-1370">Agreed</w>
     <w lemma="in" pos="acp" xml:id="B02939-001-a-1380">in</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-1390">the</w>
     <w lemma="house" pos="n1" xml:id="B02939-001-a-1400">House</w>
     <pc unit="sentence" xml:id="B02939-001-a-1410">.</pc>
    </p>
    <p xml:id="B02939-e90">
     <w lemma="that" pos="cs" xml:id="B02939-001-a-1420">That</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-1430">the</w>
     <w lemma="same" pos="d" xml:id="B02939-001-a-1440">same</w>
     <w lemma="shall" pos="vmb" xml:id="B02939-001-a-1450">shall</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-1460">be</w>
     <w lemma="manage" pos="vvn" xml:id="B02939-001-a-1470">Managed</w>
     <w lemma="by" pos="acp" xml:id="B02939-001-a-1480">by</w>
     <w lemma="a" pos="d" xml:id="B02939-001-a-1490">a</w>
     <w lemma="number" pos="n1" xml:id="B02939-001-a-1500">Number</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-1510">of</w>
     <w lemma="person" pos="n2" xml:id="B02939-001-a-1520">Persons</w>
     <pc join="right" xml:id="B02939-001-a-1530">(</pc>
     <w lemma="not" pos="xx" xml:id="B02939-001-a-1540">not</w>
     <w lemma="exceed" pos="vvg" xml:id="B02939-001-a-1550">exceeding</w>
     <w lemma="forty" pos="crd" xml:id="B02939-001-a-1560">Forty</w>
     <pc xml:id="B02939-001-a-1570">)</pc>
     <w lemma="to" pos="prt" xml:id="B02939-001-a-1580">to</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-1590">be</w>
     <w lemma="choose" pos="vvn" xml:id="B02939-001-a-1600">Chosen</w>
     <w lemma="by" pos="acp" xml:id="B02939-001-a-1610">by</w>
     <w lemma="parliament" pos="n1" xml:id="B02939-001-a-1620">Parliament</w>
     <pc xml:id="B02939-001-a-1630">,</pc>
     <w lemma="to" pos="acp" xml:id="B02939-001-a-1640">to</w>
     <w lemma="who" pos="crq" xml:id="B02939-001-a-1650">whom</w>
     <w lemma="they" pos="pns" xml:id="B02939-001-a-1660">they</w>
     <w lemma="shall" pos="vmb" xml:id="B02939-001-a-1670">shall</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-1680">be</w>
     <w lemma="accountable" pos="j" xml:id="B02939-001-a-1690">Accountable</w>
     <pc unit="sentence" xml:id="B02939-001-a-1700">.</pc>
    </p>
    <p xml:id="B02939-e100">
     <w lemma="that" pos="cs" xml:id="B02939-001-a-1710">That</w>
     <w lemma="that" pos="d" xml:id="B02939-001-a-1720">that</w>
     <w lemma="number" pos="n1" xml:id="B02939-001-a-1730">Number</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-1740">of</w>
     <w lemma="person" pos="n2" xml:id="B02939-001-a-1750">Persons</w>
     <w lemma="shall" pos="vmb" xml:id="B02939-001-a-1760">shall</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-1770">be</w>
     <w lemma="impower" pos="vvn" xml:id="B02939-001-a-1780">Impowered</w>
     <w lemma="to" pos="prt" xml:id="B02939-001-a-1790">to</w>
     <w lemma="take" pos="vvi" xml:id="B02939-001-a-1800">take</w>
     <w lemma="up" pos="acp" xml:id="B02939-001-a-1810">up</w>
     <w lemma="money" pos="n1" xml:id="B02939-001-a-1820">Money</w>
     <w lemma="at" pos="acp" xml:id="B02939-001-a-1830">at</w>
     <w lemma="interest" pos="n1" rend="hi" xml:id="B02939-001-a-1840">Interest</w>
     <w lemma="not" pos="xx" xml:id="B02939-001-a-1850">not</w>
     <w lemma="exceed" pos="vvg" xml:id="B02939-001-a-1860">exceeding</w>
     <w lemma="1500000" pos="crd" xml:id="B02939-001-a-1870">1500000</w>
     <w lemma="l" pos="sy" rend="abbr" xml:id="B02939-001-a-1880">l</w>
     <w lemma="for" pos="acp" xml:id="B02939-001-a-1890">for</w>
     <w lemma="carry" pos="vvg" xml:id="B02939-001-a-1900">carrying</w>
     <w lemma="on" pos="acp" xml:id="B02939-001-a-1910">on</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-1920">of</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-1930">the</w>
     <w lemma="trade" pos="n1" rend="hi" xml:id="B02939-001-a-1940">Trade</w>
     <pc unit="sentence" xml:id="B02939-001-a-1950">.</pc>
    </p>
    <p xml:id="B02939-e150">
     <w lemma="that" pos="cs" xml:id="B02939-001-a-1960">That</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-1970">the</w>
     <w lemma="stock" pos="n1" xml:id="B02939-001-a-1980">Stock</w>
     <w lemma="so" pos="av" xml:id="B02939-001-a-1990">so</w>
     <w lemma="take" pos="vvn" xml:id="B02939-001-a-2000">taken</w>
     <w lemma="up" pos="acp" xml:id="B02939-001-a-2010">up</w>
     <w lemma="and" pos="cc" xml:id="B02939-001-a-2020">and</w>
     <w lemma="employ" pos="vvn" xml:id="B02939-001-a-2030">Employed</w>
     <pc xml:id="B02939-001-a-2040">,</pc>
     <w lemma="shall" pos="vmb" xml:id="B02939-001-a-2050">shall</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-2060">be</w>
     <w lemma="liable" pos="j" xml:id="B02939-001-a-2070">liable</w>
     <w lemma="to" pos="prt" xml:id="B02939-001-a-2080">to</w>
     <w lemma="pay" pos="vvi" xml:id="B02939-001-a-2090">pay</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-2100">the</w>
     <w lemma="interest" pos="n1" rend="hi" xml:id="B02939-001-a-2110">Interest</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-2120">of</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-2130">the</w>
     <w lemma="say" pos="j-vn" xml:id="B02939-001-a-2140">said</w>
     <w lemma="money" pos="n1" xml:id="B02939-001-a-2150">Money</w>
     <w lemma="and" pos="cc" xml:id="B02939-001-a-2160">and</w>
     <w lemma="principal" pos="nn1" xml:id="B02939-001-a-2170">Principal</w>
     <w lemma="when" pos="crq" xml:id="B02939-001-a-2180">when</w>
     <w lemma="it" pos="pn" xml:id="B02939-001-a-2190">it</w>
     <w lemma="can" pos="vmb" xml:id="B02939-001-a-2200">can</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-2210">be</w>
     <w lemma="do" pos="vvn" xml:id="B02939-001-a-2220">done</w>
     <pc xml:id="B02939-001-a-2230">,</pc>
     <w lemma="and" pos="cc" xml:id="B02939-001-a-2240">and</w>
     <w lemma="that" pos="cs" xml:id="B02939-001-a-2250">that</w>
     <w lemma="there" pos="av" xml:id="B02939-001-a-2260">there</w>
     <w lemma="may" pos="vmb" xml:id="B02939-001-a-2270">may</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-2280">be</w>
     <w lemma="no" pos="dx" xml:id="B02939-001-a-2290">no</w>
     <w lemma="fear" pos="n1" xml:id="B02939-001-a-2300">fear</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-2310">of</w>
     <w lemma="raise" pos="vvg" xml:id="B02939-001-a-2320">Raising</w>
     <w lemma="money" pos="n1" xml:id="B02939-001-a-2330">Money</w>
     <w lemma="enough" pos="av-d" xml:id="B02939-001-a-2340">enough</w>
     <w lemma="to" pos="prt" xml:id="B02939-001-a-2350">to</w>
     <w lemma="well" pos="av" xml:id="B02939-001-a-2360">well</w>
     <w lemma="carry" pos="vvb" xml:id="B02939-001-a-2370">carry</w>
     <w lemma="on" pos="acp" xml:id="B02939-001-a-2380">on</w>
     <w lemma="thus" pos="av" xml:id="B02939-001-a-2390">thus</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-2400">the</w>
     <hi xml:id="B02939-e170">
      <w lemma="East-India" pos="nn1" xml:id="B02939-001-a-2410">East-India</w>
      <w lemma="trade" pos="n1" xml:id="B02939-001-a-2420">Trade</w>
     </hi>
     <pc rend="follows-hi" unit="sentence" xml:id="B02939-001-a-2430">.</pc>
    </p>
    <p xml:id="B02939-e180">
     <label xml:id="B02939-e190">
      <w join="right" lemma="it" pos="pn" xml:id="B02939-001-a-2440">'T</w>
      <w join="left" lemma="be" pos="vvz" xml:id="B02939-001-a-2441">is</w>
      <w lemma="far" pos="avc-j" xml:id="B02939-001-a-2450">farther</w>
      <w lemma="propose" pos="vvn" xml:id="B02939-001-a-2460">Proposed</w>
      <pc xml:id="B02939-001-a-2470">,</pc>
     </label>
    </p>
    <p xml:id="B02939-e200">
     <w lemma="that" pos="cs" xml:id="B02939-001-a-2480">That</w>
     <w lemma="there" pos="av" xml:id="B02939-001-a-2490">there</w>
     <w lemma="shall" pos="vmb" xml:id="B02939-001-a-2500">shall</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-2510">be</w>
     <w lemma="settle" pos="vvn" xml:id="B02939-001-a-2520">settled</w>
     <w lemma="for" pos="acp" xml:id="B02939-001-a-2530">for</w>
     <w lemma="ever" pos="av" xml:id="B02939-001-a-2540">ever</w>
     <pc xml:id="B02939-001-a-2550">,</pc>
     <w lemma="one" pos="crd" xml:id="B02939-001-a-2560">one</w>
     <w lemma="single" pos="j" xml:id="B02939-001-a-2570">single</w>
     <w lemma="month" pos="n2" xml:id="B02939-001-a-2580">Months</w>
     <w lemma="tax" pos="vvb" rend="hi" xml:id="B02939-001-a-2590">Tax</w>
     <w lemma="in" pos="acp" xml:id="B02939-001-a-2600">in</w>
     <w lemma="a" pos="d" xml:id="B02939-001-a-2610">a</w>
     <w lemma="year" pos="n1" xml:id="B02939-001-a-2620">year</w>
     <pc xml:id="B02939-001-a-2630">,</pc>
     <w lemma="after" pos="acp" xml:id="B02939-001-a-2640">after</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-2650">the</w>
     <w lemma="rate" pos="n1" xml:id="B02939-001-a-2660">rate</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-2670">of</w>
     <w lemma="70000" pos="crd" xml:id="B02939-001-a-2680">70000</w>
     <foreign xml:id="B02939-e220" xml:lang="lat">
      <w lemma="l" pos="sy" rend="abbr" xml:id="B02939-001-a-2690">l</w>
      <w lemma="per" pos="fla" xml:id="B02939-001-a-2700">per</w>
      <w lemma="mensem" pos="fla" xml:id="B02939-001-a-2710">Mensem</w>
     </foreign>
     <w lemma="as" pos="acp" xml:id="B02939-001-a-2720">as</w>
     <w lemma="a" pos="d" xml:id="B02939-001-a-2730">a</w>
     <w lemma="fund" pos="n1" xml:id="B02939-001-a-2740">Fund</w>
     <w lemma="to" pos="acp" xml:id="B02939-001-a-2750">to</w>
     <w lemma="a" pos="d" xml:id="B02939-001-a-2760">a</w>
     <w lemma="perpetual" pos="j" xml:id="B02939-001-a-2770">perpetual</w>
     <w lemma="interest" pos="n1" rend="hi" xml:id="B02939-001-a-2780">Interest</w>
     <w lemma="at" pos="acp" xml:id="B02939-001-a-2790">at</w>
     <w lemma="5" pos="crd" xml:id="B02939-001-a-2800">5</w>
     <hi xml:id="B02939-e250">
      <w lemma="per" pos="fmi" xml:id="B02939-001-a-2810">per</w>
      <w lemma="cent" pos="n1" xml:id="B02939-001-a-2820">Cent</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-2830">of</w>
     <w lemma="1400000" pos="crd" xml:id="B02939-001-a-2840">1400000</w>
     <w lemma="l" pos="sy" rend="abbr" xml:id="B02939-001-a-2850">l</w>
     <w lemma="but" pos="acp" xml:id="B02939-001-a-2860">but</w>
     <w lemma="not" pos="xx" xml:id="B02939-001-a-2870">not</w>
     <w lemma="to" pos="prt" xml:id="B02939-001-a-2880">to</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-2890">be</w>
     <w lemma="raise" pos="vvn" xml:id="B02939-001-a-2900">raised</w>
     <w lemma="but" pos="acp" xml:id="B02939-001-a-2910">but</w>
     <w lemma="when" pos="crq" xml:id="B02939-001-a-2920">when</w>
     <w lemma="find" pos="vvd" xml:id="B02939-001-a-2930">found</w>
     <w lemma="needful" pos="j" reg="needful" xml:id="B02939-001-a-2940">needfull</w>
     <w lemma="by" pos="acp" xml:id="B02939-001-a-2950">by</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-2960">the</w>
     <w lemma="person" pos="n2" xml:id="B02939-001-a-2970">Persons</w>
     <w lemma="appoint" pos="vvn" xml:id="B02939-001-a-2980">appointed</w>
     <w lemma="to" pos="acp" xml:id="B02939-001-a-2990">to</w>
     <w lemma="manage" pos="n1" xml:id="B02939-001-a-3000">Manage</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3010">the</w>
     <w lemma="trade" pos="n1" rend="hi" xml:id="B02939-001-a-3020">Trade</w>
     <pc unit="sentence" xml:id="B02939-001-a-3030">.</pc>
    </p>
    <p xml:id="B02939-e290">
     <w lemma="so" pos="av" xml:id="B02939-001-a-3040">So</w>
     <w lemma="that" pos="cs" xml:id="B02939-001-a-3050">that</w>
     <w lemma="beside" pos="acp" xml:id="B02939-001-a-3060">besides</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3070">the</w>
     <w lemma="money" pos="n1" xml:id="B02939-001-a-3080">Money</w>
     <w lemma="employ" pos="vvn" xml:id="B02939-001-a-3090">Employed</w>
     <w lemma="in" pos="acp" xml:id="B02939-001-a-3100">in</w>
     <w lemma="this" pos="d" xml:id="B02939-001-a-3110">this</w>
     <w lemma="trade" pos="n1" xml:id="B02939-001-a-3120">Trade</w>
     <pc xml:id="B02939-001-a-3130">,</pc>
     <w lemma="and" pos="cc" xml:id="B02939-001-a-3140">and</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3150">the</w>
     <w lemma="profit" pos="n1" xml:id="B02939-001-a-3160">Profit</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-3170">of</w>
     <w lemma="it" pos="pn" xml:id="B02939-001-a-3180">it</w>
     <pc xml:id="B02939-001-a-3190">,</pc>
     <w lemma="out" pos="av" xml:id="B02939-001-a-3200">out</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-3210">of</w>
     <w lemma="which" pos="crq" xml:id="B02939-001-a-3220">which</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3230">the</w>
     <w lemma="interest" pos="n1" rend="hi" xml:id="B02939-001-a-3240">Interest</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-3250">of</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3260">the</w>
     <w lemma="money" pos="n1" xml:id="B02939-001-a-3270">Money</w>
     <w lemma="to" pos="prt" xml:id="B02939-001-a-3280">to</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-3290">be</w>
     <w lemma="borrow" pos="vvn" xml:id="B02939-001-a-3300">Borrowed</w>
     <w lemma="for" pos="acp" xml:id="B02939-001-a-3310">for</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3320">the</w>
     <w lemma="carry" pos="vvg" xml:id="B02939-001-a-3330">carrying</w>
     <w lemma="it" pos="pn" xml:id="B02939-001-a-3340">it</w>
     <w lemma="on" pos="acp" xml:id="B02939-001-a-3350">on</w>
     <pc xml:id="B02939-001-a-3360">,</pc>
     <w lemma="may" pos="vmb" xml:id="B02939-001-a-3370">may</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-3380">be</w>
     <w lemma="easy" pos="av-j" xml:id="B02939-001-a-3390">easily</w>
     <w lemma="pay" pos="vvn" xml:id="B02939-001-a-3400">paid</w>
     <w lemma="here" pos="av" xml:id="B02939-001-a-3410">here</w>
     <w lemma="will" pos="vmb" xml:id="B02939-001-a-3420">will</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-3430">be</w>
     <w lemma="land" pos="n1" xml:id="B02939-001-a-3440">Land</w>
     <w lemma="security" pos="n1" xml:id="B02939-001-a-3450">Security</w>
     <w lemma="as" pos="acp" xml:id="B02939-001-a-3460">as</w>
     <w lemma="a" pos="d" xml:id="B02939-001-a-3470">a</w>
     <w lemma="perpetual" pos="j" xml:id="B02939-001-a-3480">perpetual</w>
     <w lemma="fund" pos="n1" xml:id="B02939-001-a-3490">Fund</w>
     <w lemma="for" pos="acp" xml:id="B02939-001-a-3500">for</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3510">the</w>
     <w lemma="interest" pos="n1" rend="hi" xml:id="B02939-001-a-3520">Interest</w>
     <w lemma="settle" pos="vvn" xml:id="B02939-001-a-3530">settled</w>
     <pc xml:id="B02939-001-a-3540">,</pc>
     <w lemma="so" pos="av" xml:id="B02939-001-a-3550">so</w>
     <w lemma="that" pos="cs" xml:id="B02939-001-a-3560">that</w>
     <w lemma="shall" pos="vmd" xml:id="B02939-001-a-3570">should</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3580">the</w>
     <w lemma="stock" pos="n1" rend="hi" xml:id="B02939-001-a-3590">Stock</w>
     <w lemma="fail" pos="vvi" xml:id="B02939-001-a-3600">fail</w>
     <pc xml:id="B02939-001-a-3610">,</pc>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3620">the</w>
     <w lemma="lender" pos="ng1" reg="Lender's" xml:id="B02939-001-a-3630">Lenders</w>
     <w lemma="will" pos="vmb" xml:id="B02939-001-a-3640">will</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-3650">be</w>
     <w lemma="secure" pos="j" xml:id="B02939-001-a-3660">secure</w>
     <pc xml:id="B02939-001-a-3670">,</pc>
     <w lemma="which" pos="crq" xml:id="B02939-001-a-3680">which</w>
     <w lemma="will" pos="vmb" xml:id="B02939-001-a-3690">will</w>
     <w lemma="give" pos="vvi" xml:id="B02939-001-a-3700">give</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3710">the</w>
     <w lemma="conpany" pos="n1" xml:id="B02939-001-a-3720">Conpany</w>
     <w lemma="such" pos="d" xml:id="B02939-001-a-3730">such</w>
     <w lemma="a" pos="d" xml:id="B02939-001-a-3740">a</w>
     <w lemma="credit" pos="n1" xml:id="B02939-001-a-3750">Credit</w>
     <pc xml:id="B02939-001-a-3760">,</pc>
     <w lemma="they" pos="pns" xml:id="B02939-001-a-3770">they</w>
     <w lemma="can" pos="vmb" xml:id="B02939-001-a-3780">can</w>
     <w lemma="never" pos="avx" xml:id="B02939-001-a-3790">never</w>
     <w lemma="want" pos="vvi" xml:id="B02939-001-a-3800">want</w>
     <w lemma="money" pos="n1" rend="hi" xml:id="B02939-001-a-3810">Money</w>
     <w lemma="at" pos="acp" xml:id="B02939-001-a-3820">at</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3830">the</w>
     <w lemma="low" pos="js" xml:id="B02939-001-a-3840">lowest</w>
     <w lemma="interest" pos="n1" rend="hi" xml:id="B02939-001-a-3850">Interest</w>
     <w lemma="that" pos="cs" xml:id="B02939-001-a-3860">that</w>
     <w lemma="any" pos="d" xml:id="B02939-001-a-3870">any</w>
     <w lemma="where" pos="crq" xml:id="B02939-001-a-3880">where</w>
     <w lemma="it" pos="pn" xml:id="B02939-001-a-3890">it</w>
     <w lemma="can" pos="vmb" xml:id="B02939-001-a-3900">can</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-3910">be</w>
     <w lemma="have" pos="vvn" xml:id="B02939-001-a-3920">had</w>
     <pc unit="sentence" xml:id="B02939-001-a-3930">.</pc>
     <w lemma="and" pos="cc" xml:id="B02939-001-a-3940">And</w>
     <pc xml:id="B02939-001-a-3950">,</pc>
    </p>
    <p xml:id="B02939-e350">
     <w lemma="thus" pos="av" xml:id="B02939-001-a-3960">Thus</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-3970">the</w>
     <w lemma="profit" pos="n2" xml:id="B02939-001-a-3980">Profits</w>
     <w lemma="first" pos="ord" xml:id="B02939-001-a-3990">first</w>
     <w lemma="arise" pos="vvg" xml:id="B02939-001-a-4000">arising</w>
     <pc xml:id="B02939-001-a-4010">,</pc>
     <w lemma="may" pos="vmb" xml:id="B02939-001-a-4020">may</w>
     <w lemma="go" pos="vvi" xml:id="B02939-001-a-4030">go</w>
     <w lemma="to" pos="prt" xml:id="B02939-001-a-4040">to</w>
     <w lemma="clear" pos="vvi" xml:id="B02939-001-a-4050">clear</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-4060">the</w>
     <w lemma="money" pos="n1" rend="hi" xml:id="B02939-001-a-4070">Money</w>
     <w lemma="first" pos="ord" xml:id="B02939-001-a-4080">first</w>
     <w lemma="borrow" pos="vvd" xml:id="B02939-001-a-4090">Borrowed</w>
     <pc xml:id="B02939-001-a-4100">,</pc>
     <w lemma="and" pos="cc" xml:id="B02939-001-a-4110">and</w>
     <w lemma="after" pos="acp" xml:id="B02939-001-a-4120">after</w>
     <w lemma="that" pos="d" xml:id="B02939-001-a-4130">that</w>
     <w lemma="be" pos="vvz" xml:id="B02939-001-a-4140">is</w>
     <w lemma="pay" pos="vvn" xml:id="B02939-001-a-4150">paid</w>
     <pc xml:id="B02939-001-a-4160">,</pc>
     <w lemma="to" pos="prt" xml:id="B02939-001-a-4170">to</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-4180">the</w>
     <w lemma="owner" pos="n2" xml:id="B02939-001-a-4190">Owners</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-4200">of</w>
     <w lemma="land" pos="n1" xml:id="B02939-001-a-4210">Land</w>
     <w lemma="in" pos="acp" xml:id="B02939-001-a-4220">in</w>
     <w lemma="proportion" pos="n2" xml:id="B02939-001-a-4230">Proportions</w>
     <w lemma="to" pos="acp" xml:id="B02939-001-a-4240">to</w>
     <w lemma="their" pos="po" xml:id="B02939-001-a-4250">their</w>
     <w lemma="estate" pos="n2" xml:id="B02939-001-a-4260">Estates</w>
     <w lemma="as" pos="acp" xml:id="B02939-001-a-4270">as</w>
     <w lemma="rate" pos="vvn" xml:id="B02939-001-a-4280">Rated</w>
     <w lemma="to" pos="acp" xml:id="B02939-001-a-4290">to</w>
     <w lemma="this" pos="d" xml:id="B02939-001-a-4300">this</w>
     <w lemma="single" pos="j" xml:id="B02939-001-a-4310">single</w>
     <w lemma="month" pos="n2" xml:id="B02939-001-a-4320">Months</w>
     <w lemma="tax" pos="vvb" xml:id="B02939-001-a-4330">Tax</w>
     <pc xml:id="B02939-001-a-4340">;</pc>
     <w lemma="and" pos="cc" xml:id="B02939-001-a-4350">and</w>
     <w lemma="it" pos="pn" xml:id="B02939-001-a-4360">it</w>
     <w lemma="may" pos="vmb" xml:id="B02939-001-a-4370">may</w>
     <w lemma="be" pos="vvi" xml:id="B02939-001-a-4380">be</w>
     <w lemma="hence" pos="av" xml:id="B02939-001-a-4390">hence</w>
     <w lemma="hope" pos="vvn" xml:id="B02939-001-a-4400">hoped</w>
     <pc xml:id="B02939-001-a-4410">,</pc>
     <w lemma="that" pos="cs" xml:id="B02939-001-a-4420">that</w>
     <w lemma="after" pos="acp" xml:id="B02939-001-a-4430">after</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-4440">the</w>
     <w lemma="first" pos="ord" xml:id="B02939-001-a-4450">first</w>
     <w lemma="five" pos="crd" xml:id="B02939-001-a-4460">five</w>
     <w lemma="or" pos="cc" xml:id="B02939-001-a-4470">or</w>
     <w lemma="six" pos="crd" xml:id="B02939-001-a-4480">six</w>
     <w lemma="year" pos="n2" xml:id="B02939-001-a-4490">years</w>
     <pc xml:id="B02939-001-a-4500">,</pc>
     <w lemma="without" pos="acp" xml:id="B02939-001-a-4510">without</w>
     <w lemma="pay" pos="vvg" xml:id="B02939-001-a-4520">paying</w>
     <w lemma="one" pos="crd" xml:id="B02939-001-a-4530">one</w>
     <w lemma="penny" pos="n1" xml:id="B02939-001-a-4540">Penny</w>
     <pc xml:id="B02939-001-a-4550">,</pc>
     <w lemma="the" pos="d" xml:id="B02939-001-a-4560">the</w>
     <w lemma="nation" pos="n1" xml:id="B02939-001-a-4570">Nation</w>
     <w lemma="in" pos="acp" xml:id="B02939-001-a-4580">in</w>
     <w lemma="general" pos="n1" xml:id="B02939-001-a-4590">general</w>
     <w lemma="may" pos="vmb" xml:id="B02939-001-a-4600">may</w>
     <w lemma="have" pos="vvi" xml:id="B02939-001-a-4610">have</w>
     <w lemma="the" pos="d" xml:id="B02939-001-a-4620">the</w>
     <w lemma="whole" pos="j" xml:id="B02939-001-a-4630">whole</w>
     <w lemma="benefit" pos="n1" xml:id="B02939-001-a-4640">Benefit</w>
     <w lemma="of" pos="acp" xml:id="B02939-001-a-4650">of</w>
     <w lemma="this" pos="d" xml:id="B02939-001-a-4660">this</w>
     <w lemma="great" pos="j" xml:id="B02939-001-a-4670">great</w>
     <w lemma="East-India" pos="nn1" rend="hi" xml:id="B02939-001-a-4680">East-India</w>
     <w lemma="trade" pos="n1" xml:id="B02939-001-a-4690">Trade</w>
     <pc unit="sentence" xml:id="B02939-001-a-4700">.</pc>
    </p>
   </div>
  </body>
 </text>
</TEI>
